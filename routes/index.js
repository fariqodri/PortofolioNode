var express = require('express');
var router = express.Router();
var response = {};
response['nama'] = "Fari Qodri Andana";
response['title'] = response.nama;
response['story'] = "Let\'s try new stuff";

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', response);
});

module.exports = router;
